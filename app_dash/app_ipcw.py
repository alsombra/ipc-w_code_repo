# Author: Renato Santos Aranha
# Projeto IPC-W FGV
# Last version: 03/10/2018

import pickle
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_auth
import plotly.graph_objs as go
import numpy as np
import pandas as pd
import dash_table_experiments as dt
from nltk.tokenize import word_tokenize
import gera_inflacao_mes as gim
import nltk
import os
import classifier as clf
nltk.download('punkt')


login_list = [
    ['ipc-w', 'ipc-web-123'],
    ['thayssa', 'ipc-web-123'],
    ['pedro','ipc-web-123'],
    ['natan', 'ipc-web-123'],
    ['daniel', 'ipc-web-123']
]

app = dash.Dash('auth')
#app = dash.Dash(__name__)


auth = dash_auth.BasicAuth(
    app,
    login_list
)


config = app.config['suppress_callback_exceptions'] = True
server = app.server


# LOADING DATA
# Read data for tables (one df per table)

df = round(gim.gera_inflacao_mes(gim.price_pivot, subitem=None, ano_mes=None), 5)
ppivot = gim.price_pivot  # CARREGAR A base correta  - "base-pre-classificacao"


base_path = '../../base_pre_classificacao.csv'
base_raw = clf.load_base_raw(base_path)
print('carregou base_raw', base_raw.head(1))

ipc_lista = pd.read_csv('./data/ipc_subitens_final.csv')

#loading subitem_dict
#Dict { subitem (nome original) : subitem(nome na base) }
with open('./data/subitens_dict.pickle', 'rb') as file:
    dict_subitem_names = pickle.load(file)

#loading subitem_id_dict
#Dict { subitem (nome na base) : subitem_id }
with open('./data/subitens_id_dict.pickle', 'rb') as file:
    dict_subitem_id = pickle.load(file)

#Lista usada pra Dropdowns e multiple choice icons { subitem (nome original) : subitem(nome na base) }
lista_subitem_name = [{'label': key, 'value': dict_subitem_names[key]} for key in np.sort(list(dict_subitem_names.keys()))]


#Dicionário { subitem : subitem_id }
alimentos_lista = ipc_lista[["nome_subitem", "id_subitem"]]
alimentos_lista = alimentos_lista[alimentos_lista["nome_subitem"].isin(list(dict_subitem_names.keys()))]

# REUSABLE COMPONENTS

tab_selected_style = {
    'borderTop': '1px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'font-size': 20,
    'fontWeight': 'bold'

}

whitelist_style = {
    'font-size':'10',
    'padding-right': '10px',
    'width': '100%',
    'color': 'blue'}

blacklist_style = {
    'border': '1px solid black',
    'font-size':'10',
    'width': '540px',
    'padding': '10px',
    'height': '400px',
    'overflow':'scroll'
}

products_style ={
    'border': '1px solid black',
    'font-size':'10',
    'padding': '10px',
    'width': '100%',
    'height': '500px',
    'position': 'center',
    'overflow':'scroll',
    'margin':'100 auto'

}

stats_style = {
    'border': '1px solid black',
    'font-size':'10',
    'padding': '10px',
    'width': '100%',
    'height': '150px',
    'position': 'center',
    'overflow':'scroll',
    'margin':'100 auto'

}

dbutton_style = {
    'background-color': 'black',
    'color': 'white'
}

abutton_style = {
    'background-color': 'green',
    'color': 'white'
}

sbutton_style = {
            'background-color': '#FF5A4F',
            'color': 'white'
}
fbutton_style = {
    'background-color': '#252BFF',
    'color': 'white'
}


list_of_itens = [html.P(["nome "+ str(i) + ' na whitelist']) for i in range(10)]


def filter_subitem(subitens):
    df1 = df[df.ipc_subitem_name.isin(subitens)]
    return df1


def print_pdf_button():
    printButton = html.A(['Print PDF']
                         , className="button no-print print"
                         , style={'position': "absolute", 'right': '117'}
                         )
    return printButton


# includes page/full view
def get_logo():
    logo = html.Div([

        html.Div([
            html.A(
                html.Img(src='http://bibliotecadigital.fgv.br/dspace/bitstream/id/45381/?sequence=-1',
                         style={'width': '130px'}),
                href='https://emap.fgv.br/',
                target="_blank"),
            print_pdf_button()
        ], className="ten columns padded")

    ], className="row gs-header")
    return logo


def get_header():
    header = html.Div([

        html.Div([
            html.H5(
                'IPCW - Painel do projeto')
        ], className="twelve columns padded")

    ], className="row gs-header gs-text-header")
    return header


def get_menu():
    menu = html.Div([

        dcc.Link('Overview   ', href='/overview', className="tab first"),

        dcc.Link('Price Performance   ', href='/price-performance', className="tab"),

        dcc.Link('Classificação   ', href='/classificacao', className="tab")
    ], className="row ", style={'font-size': 12, 'padding': 10, 'margin-bottom': 10})
    return menu

pricePerformance = html.Div([
    html.Div([

        # Header
        get_logo(),
        get_header(),
        html.Br([]),
        get_menu(),

        # Selector

        html.Div([
            dcc.RadioItems(
                id='options_FULL_rd',
                options=[
                    {'label': 'Indices por subitem', 'value': 'FULL'},
                    {'label': 'Raw Data', 'value': 'RD'}
                ],
                value='FULL',
                labelStyle={'width': '13%', 'display': 'inline-block'}
            )
        ]),

        html.Div(id='test_',
                 className="row"
                 ),

    ], className="row")

], className="ten columns offset-by-one")

subitem = 'acem' #Placeholder inicial

classifica = html.Div([

    html.Div([

        # Header
        get_logo(),
        get_header(),
        html.Br([]),
        get_menu(),

        # Tabs

        html.Div([
            html.Div(children=[dcc.Dropdown(
                options=lista_subitem_name,
                value='acem',
                id='dropdown-lists')],
                style={'width': '100%', 'padding': 10})
            ,
            html.Div(id='conjuntao', className="twelve columns padded",
                     style={'padding': 10, 'width': '100%', 'margin': '0 auto'}, children=[
                    html.Div(id='filter-table', children=dt.DataTable(rows=[{}]),
                             style={'width': '450px', 'float': 'left'}),
                    html.Div(id='teste_1245',
                             style={'margin': '0 auto', 'float': 'right', 'width': '550px', 'padding': '0px'},
                             children=[dcc.Tabs(id="tabs", value='tab-w',
                                                style={'font-size': 15,
                                                       'width': '540px',
                                                       'float': 'right'},
                                                children=[
                                                    dcc.Tab(id='wl', label='Whitelist', value='tab-w',
                                                            selected_style=tab_selected_style,
                                                            children = [dcc.Input(id = 'wl_input', type = 'text', value = ''),
                                                                        html.Div(id = 'wl_container',
                                                                                 style = whitelist_style)]),
                                                    dcc.Tab(id='bl', label='Blacklist', value='tab-b',
                                                            selected_style=tab_selected_style,
                                                            children=[html.Div(id = 'input_container', style = {'margin': '0 auto'},
                                                                      children = [
                                                                      dcc.Input(id = 'bl_dept_input', type = 'text',
                                                                                style={'float': 'left'}, value = ''),
                                                                      dcc.Input(id='bl_name_input', type='text',
                                                                                style = { 'float': 'right', 'margin-right': '10px'}, value='')]),
                                                                      html.Div(id='bl_container',
                                                                               className='tabs-content',
                                                                               style=blacklist_style,
                                                                               children=[
                                                                                   html.Div(
                                                                                       id='bl_container_dept_title',
                                                                                       children=html.H5(
                                                                                           ["Department: "],
                                                                                           style={
                                                                                               'width': '230px',
                                                                                               'float': 'left',
                                                                                               'font-weight': 'bold',
                                                                                               'color': 'red'
                                                                                           })),
                                                                                   html.Div(
                                                                                       id='bl_container_name_title',
                                                                                       children=html.H5(["Name: "],
                                                                                                        style={
                                                                                                        'width': '230px',
                                                                                                        'float': 'right',
                                                                                                        'font-weight': 'bold',
                                                                                                        'color': 'black'
                                                                                                        }
                                                                                                        )),
                                                                                   html.Div(
                                                                                       id='bl_container_department',
                                                                                       style={'width': '235px',
                                                                                              'float': 'left',
                                                                                              'overflow': 'scroll',
                                                                                              'padding': '10px',
                                                                                              'color': 'red'}),
                                                                                   html.Div(id='bl_container_name',
                                                                                            style={'width': '235px',
                                                                                                   'float': 'right',
                                                                                                   'overflow': 'scroll',
                                                                                                   'padding': '20px',
                                                                                                   'color': 'black'})
                                                                               ]
                                                                               )
                                                                      ]),

                                                ]
                                                ),
                                       html.Button('Del', id = 'dbutton', style = dbutton_style, n_clicks_timestamp=0),
                                       html.Button('Add', id = 'abutton', style = abutton_style, n_clicks_timestamp=0),
                                       html.Button('Salvar Black e White list', id='sbutton',
                                                   style=sbutton_style, n_clicks_timestamp=0),
                                       html.Button('Filtrar', id='fbutton',style=fbutton_style, n_clicks_timestamp=0)
                                       ])
                ])
            ,
            html.Div(id='hidden-div', style={'display':'none'}),
            html.Div(id='products_container', children=html.H4(["AQUI FICAM OS PRODUTOS!"]),
                     style={'height': '100px', 'float': 'bottom'})
        ]),
    ], className="row")
], className="ten columns offset-by-one")

noPage = html.Div([

    html.Div([
        # Header
        get_logo(),
        get_header(),
        html.Br([]),
        get_menu(),
        html.Div([
            html.P(["Aba Overview."]),
            html.P(["Equipe IPCW: Ainda estamos trabalhando nesse conteúdo."])
        ], className="no-page"),
    ], className="row")
], className="ten columns offset-by-one")

# Describe the layout, or the UI, of the app
app.layout = html.Div([
    dcc.Location(id='url', pathname='', refresh=False),
    html.Div(id='page-content'),
    html.Div(dt.DataTable(rows=[{}]), style={'display': 'none'})
])


# CALLBACKS SECTION

# SHOW PRODUTOS FILTRADOS


@app.callback(dash.dependencies.Output('products_container', 'children'),
              [dash.dependencies.Input('fbutton', 'n_clicks_timestamp'),
              dash.dependencies.Input('dropdown-lists', 'value')])
def on_click(n_clicks_timestamp, dropdown_value):
    subitem_name = dropdown_value
    subitem_id = dict_subitem_id[subitem]
    print('entrou no callback do SHOW PRODUTOS FILTRADOS')
    print('dropdown value: ', subitem)
    print('subitem_id: ', subitem_id)

    if n_clicks_timestamp > 0:
        # LOAD WHITE_LIST
        with open('black_white_sublists/' + subitem_name + '_whitelist.pickle', 'rb') as file:
            subitem_whitelist_loaded = pickle.load(file)
        print(subitem_whitelist_loaded)
        # LOAD BLACK_LIST
        with open('black_white_sublists/' + subitem_name + '_blacklist.pickle', 'rb') as file:
            subitem_blacklist_loaded = pickle.load(file)
        #Filter

        data_summary_websites = set(base_raw['website'])

        #White filter
        base_subitem = clf.white_filter(base_raw, subitem_whitelist_loaded)

        #Black filter
        index_subitem = clf.index_after_blacklist(base_subitem, subitem_name, subitem_blacklist_loaded)

        sku_subitem = base_subitem.iloc[index_subitem].copy()
        sku_subitem['url2'] = sku_subitem['url'].apply(lambda x: '<a href="{0}">link</a>'.format(x))

        # registro do sumário do resultado da blacklist
        sku_subitem_summary_blacklist__results = len(sku_subitem)


        print('Websites sem URLs depois da Blacklist')
        print(data_summary_websites - set(sku_subitem['website']))

        df_statistics = sku_subitem.groupby(['website', 'department'])['name'].count()

        total = [str(df_statistics.index[i][0]) + str(df_statistics.index[i][1]) + " : " + str(df_statistics.iloc[i]) for i in range(len(df_statistics))]
        print(total)

        df_prods = sku_subitem[['sku', 'name', 'department', 'website', 'url']]

        prods_table = html.Div([dt.DataTable(
             id='prods_table_id',
             rows=df_prods.to_dict('records'),
             columns=sorted(df_prods.columns),
             min_width=1000,
             row_selectable=True,
             resizable=True,
             editable=False,
             filterable=True,
             sortable=True,
             selected_row_indices=[])])

        layout = [html.H4(["Produtos Filtrados de " + dropdown_value], style={ 'text-align': 'center', 'color': '#E86F0C'}),
                  html.Div(id = "filtered_products_cotainer", style = stats_style, children = [html.P(["subitem: {} \n"
                                                                                                         "subitem_id: {}".
                                                                                                        format(subitem,subitem_id)]),
                                                                                                  html.P(
                                                                                                      'URLs encontradas para o subitem {}: {:,}'.format(subitem_name, len(base_subitem))),
                                                                                                  html.P(
                                                                                                      'URLs depois da blacklist: {}/{}'.format(sku_subitem_summary_blacklist__results, len(base_subitem)),
                                                                                                  style = {'font-size': 12}),
                                                                                                  # html.P(
                                                                                                  #     'Resultados por Website e Departamento:'
                                                                                                  # ),
                                                                                                  html.P('Websites sem URLs depois da Blacklist :'),
                                                                                                  html.P(str(data_summary_websites - set(sku_subitem['website'])))]),
                  html.Div(id = 'produtos',
                      children =[html.P('Produtos'),
                                 prods_table
                                 ]
                      , style = products_style)
                  ]
        return layout



# BUTTON SALVAR BLACK & WHITE LISTS

@app.callback(dash.dependencies.Output('hidden-div', 'children'),
              [dash.dependencies.Input('sbutton', 'n_clicks_timestamp'),
              dash.dependencies.Input('dropdown-lists', 'value')])
def salva(n_clicks_timestamp, dropdown_value):
    subitem_whitelist_name = dropdown_value
    subitem_blacklist_name = dropdown_value
    if int(n_clicks_timestamp) > 0:
        #Loads TEMPs
        if os.path.isfile('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle'):
            with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle', 'rb') as file:
                subitem_whitelist_loaded = pickle.load(file)
        else:
            with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist.pickle', 'rb') as file:
                subitem_whitelist_loaded = pickle.load(file)
        if os.path.isfile('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle'):
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle', 'rb') as file:
                subitem_blacklist_loaded = pickle.load(file)
        else:
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'rb') as file:
                subitem_blacklist_loaded = pickle.load(file)
        #Saves to Official
        with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist.pickle', 'wb') as handle:
            pickle.dump(subitem_whitelist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'wb') as handle:
            pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print('saved black_whitelist')
        return 'Saved Black_White List'


# Callback pra resetar a white e black lists (temporárias)

@app.callback(dash.dependencies.Output('abutton', 'n_clicks_timestamp'),
              [dash.dependencies.Input('dropdown-lists', 'value')])
def zera(value):
    return 0

@app.callback(dash.dependencies.Output('sbutton', 'n_clicks_timestamp'),
              [dash.dependencies.Input('dropdown-lists', 'value')])
def zera(value):
    return 0

@app.callback(dash.dependencies.Output('fbutton', 'n_clicks_timestamp'),
              [dash.dependencies.Input('dropdown-lists', 'value')])
def zera(value):
    return 0

@app.callback(dash.dependencies.Output('dbutton', 'n_clicks_timestamp'),
              [dash.dependencies.Input('dropdown-lists', 'value')])
def zera(value):
    return 0

#DEL BUTTON WHITELIST

# OBS: USES EVENT --> POSSIBLY DEPRECATED IN THE FUTURE

@app.callback(dash.dependencies.Output('white_table_id', 'rows'),
              [],
              [dash.dependencies.State('tabs', 'value'),
               dash.dependencies.State('white_table_id', 'selected_row_indices'),
               dash.dependencies.State('abutton', 'n_clicks_timestamp'),
               dash.dependencies.State('dropdown-lists', 'value'),
               dash.dependencies.State('dbutton', 'n_clicks_timestamp')],
               [dash.dependencies.Event('dbutton', 'click')])
def del_butt(tab_value, selected_rows, a_n_clicks_timestamp, dropdown_value, d_n_clicks_timestamp):
    subitem_whitelist_name = dropdown_value
    if tab_value == 'tab-w':
        # DEL FUNCTIONALITY
        print('ENTROU NO LOOP DO DEL BUTTON')
        if int(d_n_clicks_timestamp) > int(a_n_clicks_timestamp):
            print('entrou no loop do del')
            if not os.path.isfile('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle'):
                with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist.pickle', 'rb') as file:
                    subitem_whitelist_loaded = pickle.load(file)
                with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle', 'wb') as handle:
                    pickle.dump(subitem_whitelist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle', 'rb') as file:
                subitem_whitelist_loaded = pickle.load(file)
            if subitem_whitelist_loaded != []:
                subitem_whitelist_loaded = [subitem_whitelist_loaded[i] for i in range(len(subitem_whitelist_loaded)) if i not in selected_rows]
                print('selected_rows: ', selected_rows)
            else:
                return html.Div('ERROR - NÃO DELETE LISTAS VAZIAS!')
            print('passou do loop do del e vai montar a white table')
            df_whitelist = pd.DataFrame({'whitelist': [i for i in subitem_whitelist_loaded]})

            # SALVO TEMP LIST
            with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle', 'wb') as handle:
                pickle.dump(subitem_whitelist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print('fim do loop do del')
            return df_whitelist.to_dict('records')



#DEL BUTTON BLACKLIST - DEPARTMENT

# OBS: USES EVENT --> POSSIBLY DEPRECATED IN THE FUTURE

@app.callback(dash.dependencies.Output('black_table_dept_id', 'rows'),
              [],
              [dash.dependencies.State('tabs', 'value'),
               dash.dependencies.State('black_table_dept_id', 'selected_row_indices'),
               dash.dependencies.State('abutton', 'n_clicks_timestamp'),
               dash.dependencies.State('dropdown-lists', 'value'),
               dash.dependencies.State('dbutton', 'n_clicks_timestamp')],
               [dash.dependencies.Event('dbutton', 'click')])
def del_butt(tab_value, selected_rows, a_n_clicks_timestamp, dropdown_value, d_n_clicks_timestamp):
    subitem_blacklist_name = dropdown_value
    if tab_value == 'tab-b':
        # DEL FUNCTIONALITY
        print('ENTROU NO LOOP DO DEL BUTTON - BLACK DEPARTMENT')
        if int(d_n_clicks_timestamp) > int(a_n_clicks_timestamp):
            print('entrou no loop do del')
            if not os.path.isfile('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle'):
                # se a temp não existe abre a original
                with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'rb') as file:
                    subitem_blacklist_loaded = pickle.load(file)
                # salva a temp como uma copia da original
                with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                          'wb') as handle:
                    pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
            # carrega a temp
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                      'rb') as file:
                subitem_blacklist_loaded = pickle.load(file)
                subitem_blacklist_dept_loaded = list(subitem_blacklist_loaded["department"])
            if subitem_blacklist_dept_loaded != []:
                subitem_blacklist_dept_loaded = [subitem_blacklist_dept_loaded[i] for i in range(len(subitem_blacklist_dept_loaded)) if i not in selected_rows]
                print('selected_rows: ', selected_rows)
            else:
                return html.Div('ERROR - NÃO DELETE LISTAS VAZIAS!')
            print('passou do loop do del e vai montar a black_dept table')
            df_blacklist_dept = pd.DataFrame({'Department': [i for i in subitem_blacklist_dept_loaded]})

            # SALVO TEMP LIST
            subitem_blacklist_loaded["department"] = subitem_blacklist_dept_loaded
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle', 'wb') as handle:
                pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print('fim do loop do del da blacklist dept')
            return df_blacklist_dept.to_dict('records')


#DEL BUTTON BLACKLIST - NAME

# OBS: USES EVENT --> POSSIBLY DEPRECATED IN THE FUTURE

@app.callback(dash.dependencies.Output('black_table_name_id', 'rows'),
              [],
              [dash.dependencies.State('tabs', 'value'),
               dash.dependencies.State('black_table_name_id', 'selected_row_indices'),
               dash.dependencies.State('abutton', 'n_clicks_timestamp'),
               dash.dependencies.State('dropdown-lists', 'value'),
               dash.dependencies.State('dbutton', 'n_clicks_timestamp')],
               [dash.dependencies.Event('dbutton', 'click')])
def del_butt(tab_value, selected_rows, a_n_clicks_timestamp, dropdown_value, d_n_clicks_timestamp):
    subitem_blacklist_name = dropdown_value
    if tab_value == 'tab-b':
        # DEL FUNCTIONALITY
        print('ENTROU NO LOOP DO DEL BUTTON - BLACK NAME')
        if int(d_n_clicks_timestamp) > int(a_n_clicks_timestamp):
            print('entrou no loop do del da BLACK NAME')
            if not os.path.isfile('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle'):
                # se a temp não existe abre a original
                with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'rb') as file:
                    subitem_blacklist_loaded = pickle.load(file)
                # salva a temp como uma copia da original
                with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                          'wb') as handle:
                    pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
            # carrega a temp
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                      'rb') as file:
                subitem_blacklist_loaded = pickle.load(file)
                subitem_blacklist_name_loaded = list(subitem_blacklist_loaded["name"])
            if subitem_blacklist_name_loaded != []:
                subitem_blacklist_name_loaded = [subitem_blacklist_name_loaded[i] for i in range(len(subitem_blacklist_name_loaded)) if i not in selected_rows]
                print('selected_rows: ', selected_rows)
            else:
                return html.Div('ERROR - NÃO DELETE LISTAS VAZIAS!')
            print('passou do loop do del e vai montar a black_name table')
            df_blacklist_name = pd.DataFrame({'Name': [i for i in subitem_blacklist_name_loaded]})

            # SALVO TEMP LIST
            subitem_blacklist_loaded["name"] = subitem_blacklist_name_loaded
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle', 'wb') as handle:
                pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
            print('fim do loop do del da blacklist name')
            return df_blacklist_name.to_dict('records')


# SHOW WHITELIST + ADD BUTTON

@app.callback(dash.dependencies.Output('wl_container', 'children'),
              [dash.dependencies.Input('tabs', 'value'),
               dash.dependencies.Input('abutton', 'n_clicks_timestamp'),
               dash.dependencies.Input('dropdown-lists', 'value')],
              [dash.dependencies.State('wl_input', 'value'),
               dash.dependencies.State('dbutton', 'n_clicks_timestamp')])
def func(tab_value, a_n_clicks_timestamp, dropdown_value, input_value, d_n_clicks_timestamp):
    subitem_whitelist_name = dropdown_value
    print('ENTROU NO CALLBACK DA WHITELIST + ADD BUTTON')
    print('dropdown_value pro whitelist', dropdown_value)
    if tab_value == 'tab-w':
        if a_n_clicks_timestamp == 0:
            with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist.pickle', 'rb') as file:
                subitem_whitelist_loaded = pickle.load(file)
                subitem_whitelist_loaded = list(subitem_whitelist_loaded)

        # ADD FUNCTIONALITY
        if int(a_n_clicks_timestamp) > int(d_n_clicks_timestamp):
            if a_n_clicks_timestamp > 0:
                if not os.path.isfile('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle'):
                    with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist.pickle', 'rb') as file:
                        subitem_whitelist_loaded = pickle.load(file)
                    with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle', 'wb') as handle:
                        pickle.dump(subitem_whitelist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
                with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle', 'rb') as file:
                    subitem_whitelist_loaded = pickle.load(file)
                    print('valor da subitem_whitelist_loaded: ', subitem_whitelist_loaded)
                    if input_value not in ['NoneType', '', ' ', '  ', '   ']:
                        lista_palavras = input_value.split(',')
                        for palavra in lista_palavras:
                            if palavra not in subitem_whitelist_loaded and palavra not in ['NoneType', '', ' ', '  ', '   ']:
                                subitem_whitelist_loaded = list(subitem_whitelist_loaded)
                                subitem_whitelist_loaded.append(palavra)
        df_whitelist = pd.DataFrame({'whitelist': [i for i in subitem_whitelist_loaded]})
        white_table = html.Div([dt.DataTable(
            id='white_table_id',
            rows=df_whitelist.to_dict('records'),
            columns=sorted(df_whitelist.columns),
            min_width=540,
            row_selectable=True,
            resizable=True,
            editable=False,
            filterable=True,
            sortable=True,
            selected_row_indices=[])])
        # SALVO TEMP LIST
        with open('black_white_sublists/' + subitem_whitelist_name + '_whitelist_temp.pickle', 'wb') as handle:
            pickle.dump(subitem_whitelist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print('before returning white_table')
        return white_table



##SHOW BLACKLIST DEPARTMENT + ADD BUTTON(TAG CALLBACK)

@app.callback(dash.dependencies.Output('bl_container_department', 'children'),
              [dash.dependencies.Input('tabs', 'value'),
               dash.dependencies.Input('dropdown-lists', 'value'),
               dash.dependencies.Input('abutton', 'n_clicks_timestamp'),],
              [dash.dependencies.State('bl_dept_input', 'value'),
               dash.dependencies.State('dbutton', 'n_clicks_timestamp')])
def func(tab_value, dropdown_value,  a_n_clicks_timestamp, input_value, d_n_clicks_timestamp):
    if tab_value == 'tab-b':
        subitem_blacklist_name = dropdown_value
        print('ENTROU NO CALLBACK DA BLACKLIST + ADD BUTTON')
        print('dropdown_value pro blacklist', dropdown_value)
        if a_n_clicks_timestamp == 0:
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'rb') as file:
                subitem_blacklist_loaded = pickle.load(file)
                subitem_blacklist_dept_loaded = list(subitem_blacklist_loaded["department"])

        # ADD FUNCTIONALITY
        if int(a_n_clicks_timestamp) > int(d_n_clicks_timestamp):
            if a_n_clicks_timestamp > 0:
                if not os.path.isfile('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle'):
                    #se a temp não existe abre a original
                    with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'rb') as file:
                        subitem_blacklist_loaded = pickle.load(file)
                    #salva a temp como uma copia da original
                    with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                              'wb') as handle:
                        pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
                #carrega a temp
                with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                          'rb') as file:
                    subitem_blacklist_loaded = pickle.load(file)
                    subitem_blacklist_dept_loaded = list(subitem_blacklist_loaded["department"])
                    print('valor da subitem_blacklist_dept_loaded: ', subitem_blacklist_dept_loaded)
                    if input_value not in ['NoneType', '', ' ', '  ', '   ']:
                        lista_palavras = input_value.split(',')
                        for palavra in lista_palavras:
                            if palavra not in subitem_blacklist_dept_loaded and palavra not in ['NoneType', '', ' ',
                                                                                           '  ', '   ']:
                                subitem_blacklist_dept_loaded = list(subitem_blacklist_dept_loaded)
                                subitem_blacklist_dept_loaded.append(palavra)
        df_blacklist_dept = pd.DataFrame({'Department': [i for i in subitem_blacklist_dept_loaded]})
        black_dept_table = html.Div([dt.DataTable(
            id='black_table_dept_id',
            rows=df_blacklist_dept.to_dict('records'),
            columns=sorted(df_blacklist_dept.columns),
            min_width=230,
            row_selectable=True,
            resizable=True,
            editable=False,
            filterable=True,
            sortable=True,
            selected_row_indices=[])])
        # SALVO TEMP LIST
        subitem_blacklist_loaded["department"] = subitem_blacklist_dept_loaded
        with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle', 'wb') as handle:
            pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print('before returning black_dept_table')
        return black_dept_table


#SHOW BLACKLIST NAME + ADD BUTTON


##SHOW BLACKLIST DEPARTMENT + ADD BUTTON(TAG CALLBACK)

@app.callback(dash.dependencies.Output('bl_container_name', 'children'),
              [dash.dependencies.Input('tabs', 'value'),
               dash.dependencies.Input('dropdown-lists', 'value'),
               dash.dependencies.Input('abutton', 'n_clicks_timestamp'),],
              [dash.dependencies.State('bl_name_input', 'value'),
               dash.dependencies.State('dbutton', 'n_clicks_timestamp')])
def func(tab_value, dropdown_value,  a_n_clicks_timestamp, input_value, d_n_clicks_timestamp):
    if tab_value == 'tab-b':
        subitem_blacklist_name = dropdown_value
        print('ENTROU NO CALLBACK DA BLACKLIST NAME + ADD BUTTON')
        print('dropdown_value pro blacklist', dropdown_value)
        if a_n_clicks_timestamp == 0:
            with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'rb') as file:
                subitem_blacklist_loaded = pickle.load(file)
                subitem_blacklist_name_loaded = list(subitem_blacklist_loaded["name"])

        # ADD FUNCTIONALITY
        if int(a_n_clicks_timestamp) > int(d_n_clicks_timestamp):
            if a_n_clicks_timestamp > 0:
                if not os.path.isfile('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle'):
                    #se a temp não existe abre a original
                    with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist.pickle', 'rb') as file:
                        subitem_blacklist_loaded = pickle.load(file)
                    #salva a temp como uma copia da original
                    with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                              'wb') as handle:
                        pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
                #carrega a temp
                with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle',
                          'rb') as file:
                    subitem_blacklist_loaded = pickle.load(file)
                    subitem_blacklist_name_loaded = list(subitem_blacklist_loaded["name"])
                    print('valor da subitem_blacklist_name_loaded: ', subitem_blacklist_name_loaded)
                    if input_value not in ['NoneType', '', ' ', '  ', '   ']:
                        lista_palavras = input_value.split(',')
                        for palavra in lista_palavras:
                            if palavra not in subitem_blacklist_name_loaded and palavra not in ['NoneType', '', ' ',
                                                                                           '  ', '   ']:
                                subitem_blacklist_name_loaded = list(subitem_blacklist_name_loaded)
                                subitem_blacklist_name_loaded.append(palavra)
        df_blacklist_name = pd.DataFrame({'Name': [i for i in subitem_blacklist_name_loaded]})
        black_name_table = html.Div([dt.DataTable(
            id='black_table_name_id',
            rows=df_blacklist_name.to_dict('records'),
            columns=sorted(df_blacklist_name.columns),
            min_width=230,
            row_selectable=True,
            resizable=True,
            editable=False,
            filterable=True,
            sortable=True,
            selected_row_indices=[])])
        # SALVO TEMP LIST
        subitem_blacklist_loaded["name"] = subitem_blacklist_name_loaded
        with open('black_white_sublists/' + subitem_blacklist_name + '_blacklist_temp.pickle', 'wb') as handle:
            pickle.dump(subitem_blacklist_loaded, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print('before returning black_dept_table')
        return black_name_table



# UPDATE CLASSIFICA
@app.callback(dash.dependencies.Output('filter-table', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def update_classifica(pathname):
    if pathname == '/classificacao':
        df1 = ppivot.copy().head(100)
        df1 = df1.loc[:, 'global_product_name':'global_product_name'] # so we can use it as a dataframe of 1 column
        tokenized_names_list = [word_tokenize(df1.global_product_name[i]) for i in range(len(df1.global_product_name))]
        token_total = [token for tokenized_name in tokenized_names_list for token in tokenized_name]
        # TRATAR ESSA PARTE PRA CONTAR FREQ DIREITO!!!!
        dict_total = {i: token_total.count(i) for i in token_total}
        df = pd.DataFrame.from_dict(dict_total, orient='index').sort_values(0, axis=0, ascending=False).reset_index()
        df = df.rename(columns={df.columns[0]: 'Nome', df.columns[1]: 'Qtd'})

        table = html.Div([dt.DataTable(
            id='table_freq_id',
            rows=df.to_dict('records'),
            columns=sorted(df.columns),
            min_width=450,
            row_selectable=True,
            resizable=True,
            editable = False,
            filterable=True,
            sortable=True,
            selected_row_indices=[])
        ])
        return table

# UPDATE PAGE
@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/' or pathname == '/overview':
        return noPage
    elif pathname == '/classificacao':
        return classifica
    elif pathname == '/price-performance':
        return pricePerformance
    else:
        return noPage


# RAW DATA (exibe Pivot Table)
@app.callback(dash.dependencies.Output('test_', 'children'),
              [dash.dependencies.Input('options_FULL_rd', 'value')])
def update_radio(value):
    if value == 'FULL':
        child = [html.Div([
            html.Label('Seleção múltipla de itens:'),
            dcc.Dropdown(
                id='multi_select',
                options=lista_subitem_name,
                value=[i['value'] for i in lista_subitem_name],
                multi=True),
            html.H6(["Current Indices"], className="gs-header gs-table-header padded"),
            html.Div([html.Table(id='ipcw-table')]
                     ),
            html.H6("Performance", className="gs-header gs-table-header padded"),
            dcc.Graph(
                id='graph-4',
                config={'displayModeBar': False}
            )
        ])
        ]
        return child
    elif value == 'RD':
        table = \
            html.Div([
                dcc.Input(id='text1',
                          type='search',
                          placeholder='Digite o termo desejado',
                          value=''),
                html.Div(id='table_cut')
            ])
        return table



# UPDATE TABLE (PRICE PERFORMANCE LAYOUT)
@app.callback(
    dash.dependencies.Output('ipcw-table', 'children'),
    [dash.dependencies.Input('multi_select', 'value')])
def make_dash_table(subitens):
    print("update_table", subitens)
    if subitens is None:
        return "VAZIO"
    ''' Return a dash definition of an HTML table (using a Pandas dataframe) '''
    df1 = filter_subitem(subitens)
    df1 = df1[['ipc_subitem_name', '2017-Jun', '2017-Jul', '2017-Aug', '2017-Sep']]  # HARDCODED! **
    table = [html.Tr([html.Td(i, style={
        'backgroundColor': 'rgb(0,62,125)',
        'color': 'white',
        'font-weight': 'bold'
    }) for i in df1.columns])]
    for index, row in df1.iterrows():
        html_row = []
        for i in range(len(row)):
            html_row.append(html.Td([row[i]]))
        table.append(html.Tr(html_row))
    return table


# UPDATE GRAPH (PRICE PERFORMANCE LAYOUT)
@app.callback(
    dash.dependencies.Output('graph-4', 'figure'),
    [dash.dependencies.Input('multi_select', 'value')])
def update_figure(subitens):
    print("graph", subitens)
    if subitens is None:
        return "Vazio"
    df1 = df[df.ipc_subitem_name.isin(subitens)].reset_index(drop=True)
    x_data = ['2017-Jun', '2017-Jul', '2017-Aug', '2017-Sep'] # HARDCODED!!!!
    y_data = df1['ipc_subitem_name']
    df1 = df1[['ipc_subitem_name', '2017-Jun', '2017-Jul', '2017-Aug', '2017-Sep']] # HARDCODED!!!
    traces = []
    for i in range(len(y_data)):
        traces.append(go.Scatter(
            x=x_data,
            y=df1.T[i][1:],
            name=y_data[i]
        ))
    layout = dict(hovermode='closest',
                  autosize=True,
                  font={
                      "family": "Raleway",
                      "size": 10
                  },
                  margin={
                      "r": 40,
                      "t": 40,
                      "b": 30,
                      "l": 40
                  },
                  showlegend=True,
                  titlefont={
                      "family": "Raleway",
                      "size": 10
                  },
                  xaxis={
                      "autorange": True,
                      "range": ["2007-12-31", "2017-12-31"]
                  },
                  yaxis={
                      "autorange": False,
                      "range": [0.6, 1.5],
                      "showline": True,
                      "type": "linear",
                      "zeroline": False
                  })
    return dict(data=traces, layout=layout)


# UPDATE TABLE_RD
@app.callback(dash.dependencies.Output('table_cut', 'children'),
              [dash.dependencies.Input('text1', 'value')])
def update(value):
    df1 = ppivot.copy().head(100)
    df1 = df1.iloc[:, :4].sort_values('global_product_name') # HARDCODED PRA PEGAR SÓ AS PRIMEIRAS 5 COLUNAS (0 a 4) !!
    df1 = df1[df1.global_product_name.str.contains(value, case=0)]
    table = html.Div([html.Div('{} produtos encontrados com o termo "{}"'.format(df1.shape[0], value),
                               style={'font-size': 14, 'padding': 10}),
                      dt.DataTable(
                          id='dataTable_indices',
                          rows=df1.to_dict('records'),
                          columns=sorted(df1.columns),
                          row_selectable=True,
                          resizable=True,
                          filterable=True,
                          sortable=True,
                          selected_row_indices=[])
                      ])
    return table


external_css = ["https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css",
                "https://cdnjs.cloudflare.com/ajax/libs/skeleton/2.0.4/skeleton.min.css",
                "https://codepen.io/bcd/pen/KQrXdb.css",
                "https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"]

for css in external_css:
    app.css.append_css({"external_url": css})

external_js = ["https://code.jquery.com/jquery-3.2.1.min.js",
               "https://codepen.io/bcd/pen/YaXojL.js"]

for js in external_js:
    app.scripts.append_script({"external_url": js})

if __name__ == '__main__':
    app.run_server(debug=True)