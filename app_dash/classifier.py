import pandas as pd
import numpy as np
import csv
import pickle
from unicodedata import normalize

def load_base_raw(base_path):
  #importa o csv vindo do banco e elimina as colunas que não serão utilizadas para classificação
  data = pd.read_csv(base_path,
                     header = 0,
                     names = ['id', 'url', 'name', 'department_id','barcode',
                              'brand', 'sku', 'seller','department','department_url',
                              'website_id', 'website'],
                     index_col= 0,
                     usecols = ['id', 'url', 'name', 'department_id','barcode', 'brand','sku',
                                'seller','department','department_url', 'website_id', 'website'],
                     dtype = {'id': str, 'sku': str, 'barcode': str, 'department_id': str, 'website_id': str},
                    )

  #Eliminando endereços de Market Place
  #As URLs capturadas que são de terceiros vendendo na plataforma do e-commerce.

  data = data[data.seller.isnull()]

  #Eliminando se existem URL's sem registro do sku ou do nome (NaN)

  data = data.dropna(subset=['sku', 'name'], how = 'any')

  base_raw = data
  return base_raw

def white_filter(base_raw, subitem_whitelist_loaded):
  search_string = ""
  for i in subitem_whitelist_loaded:
      search_string = search_string + "|" + i
  search_string = "(?i)" + search_string[1:]
  base_subitem = base_raw[base_raw['name'].str.contains(search_string)]

  return base_subitem

def index_after_blacklist(base_produto, nome_produto, subitem_blacklist):  
  index_produto = []
  not_blacklisted = True

  for i in range(0, len(base_produto)):
      for word in subitem_blacklist['department']:
          if word in base_produto.iloc[i]['department'].lower():
              not_blacklisted = False
              break

      if not_blacklisted:
          for word in subitem_blacklist['name']:
              if word in base_produto.iloc[i]['name'].lower():
                  not_blacklisted = False
                  break

      if not_blacklisted:
          index_produto.append(i)

      not_blacklisted = True
  
  return index_produto

