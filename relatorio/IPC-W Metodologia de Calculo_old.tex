\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{graphics}
\usepackage{geometry}
\usepackage{color}
\usepackage{subfigure}
\usepackage[table]{xcolor}
\newcommand{\undertilde}[1]{\underset{\widetilde{}}{#1}}

\begin{document}

\title{\textbf{IPC-W: Metodologia de Cálculo}}
\date{outubro/2017}
\author{EMAp/IBRE}
\maketitle

%____ resumo ____%

\noindent O presente relatório resume a metodologia de cálculo do IPC-W, contrastando com o cálculo do IPC. Este processo de cálculo é posterior à extração e limpeza dos dados, assim como após a classificação dos produtos em categorias consonantes com o cálculo da inflação.

%____ metodologia ipc (ibre) ____%

\section{O cálculo do IPC (IBRE)}\label{metodo_ipc}

\noindent Os dados de preços utilizados no IPC possuem essencialmente cinco dimensões:

\begin{itemize}
\item \textbf{insumo}: o produto completamente especificado (nome, marca, gramatura, unidades, sabor, etc.);
\item \textbf{subitem}: classificação dos insumos com base na POF;
\item \textbf{informante}: local/empresa em que o preço foi coletado;
\item \textbf{cidade};
\item \textbf{data}.
\end{itemize}

\noindent Tanto a coleta de preços quanto o cálculo do índice são feitos \textbf{a cada dez dias} (decênio). A inflação calculada representa uma \textbf{variação percentual mensal de preços}. O cross-section é definido pelo par insumo-informante (doravante denominado \textbf{insumo informado}). O cálculo do IPC pode ser segmentado em cinco etapas:

\begin{enumerate}
\item Coleta e estruturação da base de dados do IPC;
\item Cálculo dos preços médios e dos relativos;
\item Cálculo do IPC de cada subitem em cada cidade;
\item Agregação do IPC ``nacional";
\item Agregação do IPC de cada subitem.
\end{enumerate}

\noindent Também é importante destacar os conceitos de \textbf{período base} e \textbf{período referência}. Como exemplo, considere o IPC com \textbf{data de referência} 20/julho/2017. Neste caso, define-se o período referência como o conjunto de dias de 21/junho/17 até 20/julho/17, e o período base de 21/maio/17 até 20/junho/17. Cada período corresponde a um mês.\\

\subsection{Coleta e estruturação da base de dados do IPC}

\noindent O IBRE possui mais de 100 pesquisadores devidamente treinados para a coleta dos dados, divididos em 7 unidades federativas (BA, DF, MG, PE, RJ, RS, e SP). As coletas são feitas manualmente, sendo pré-determinados (i) as datas de coleta, (ii) os informantes (supermercados, varejistas, etc.) a serem visitados, e (iii) os produtos específicos a terem preços coletados.\\

\noindent Idealmente, para cada insumo informado são coletados \textbf{três preços para cada período} (tanto o período base quanto o período referência), um preço a cada dez dias. Por exemplo: o (insumo informado) arroz parboilizado Tio João 1kg ofertado no Pão de Açúcar do Botafogo do Rio de Janeiro deve ser coletado nos dias 07, 17 e 27 de todo mês.\\

\noindent \textbf{Importante}: se faltar alguma observação (\textit{missing data}), o último dado coletado é replicado. Se o dado persistir como faltante durante \textbf{três meses consecutivamente}, o insumo informado é retirado do cálculo do IPC. Desta forma, cada insumo informado utilizado no cálculo possui três observações no período base e três observações no período referência (seja ou não fruto de replicação de preços passados).\\

\subsection{Cálculo dos preços médios e dos relativos}

\noindent Para fins de notação, denote por $P_{s,i}^{c,j}(t)$ o preço coletado manualmente do insumo $i$ do subitem $s$, informado por $j$ na cidade $c$,  coletado no dia $t$. Denote também por $\mathcal{B}$ o conjunto de dias no período base, e $\mathcal{R}$ o conjunto de dias do período referência. Inicialmente, para cada período (base e referência), é calculado um preço médio:

\begin{equation}
\begin{aligned}
P_{s,i}^{c,j}(\mathcal{X})=\frac{1}{3}\sum_{t\in\mathcal{X}}P_{s,i}^{c,j}(t) && ,\mathcal{X} = \mathcal{B},\mathcal{R}
\end{aligned}
\end{equation}

\noindent Logo calcula-se o relativo para cada insumo $i$ do subitem $s$, informado por $j$ na cidade $c$:

\begin{equation}
R_{s,i}^{c,j}=\frac{P_{s,i}^{c,j}(\mathcal{R})}{P_{s,i}^{c,j}(\mathcal{B})}
\end{equation}

\subsection{Cálculo do IPC de cada subitem em cada cidade}

\noindent Seja $J_{s,i}^c$ o número de informantes que informaram o preço do insumo $i$ do subitem $s$ na cidade $c$. Com isto, é calculada uma média geométrica dos relativos, para cada insumo $i$ do subitem $s$ na cidade $c$:

\begin{equation}\label{r_csi_ipc}
R_{s,i}^c=\left(\prod_j{R_{s,i}^{c,j}}\right)^\frac{1}{J_{s,i}^c}
\end{equation}

\noindent A última agregação deste passo faz uso de uma média geométrica ponderada. Seja $I_s^c$ o número total de insumos informados para cada subitem $s$ na cidade $c$:

\begin{equation}
I_s^c=\sum_i{J_{s,i}^c}
\end{equation}

\noindent Assim, cada insumo $i$ do subitem $s$ na cidade $c$ ganha o seguinte peso:

\begin{equation}\label{w_sic_ipc}
w_{s,i}^c=\frac{J_{s,i}^c}{I_s^c}
\end{equation}

\noindent Finalmente, cada subitem $s$ tem IPC na cidade $c$ dado por:

\begin{equation}\label{pi_cs_ipc}
\pi_s^c=\left[\prod_i{\left(R_{s,i}^c\right)^{w_{s,i}^c}}\right]-1
\end{equation}

\noindent \textbf{Nota de equivalência matemática}: como será melhor notado adiante, os cálculos envolvendo média geométrica geram os mesmos resultados nos casos online e offline, apesar do cálculo online não discriminar pesos diferentes para websites distintos (utiliza média geométrica simples). Demonstração:

\begin{equation}
1+\pi_s^c \overset{\eqref{pi_cs_ipc}}{=} \prod_i{(R_{s,i}^c)^{w_{s,i}^c}} \overset{\eqref{r_csi_ipc}}{=} \prod_i{\left[ \prod_j{(R_{s,i}^{c,j})^\frac{1}{J_{s,i}^c}} \right]^{w_{s,i}^c}} \overset{\eqref{w_sic_ipc}}{=} \left(\prod_{i,j}{R_{s,i}^{c,j}}\right)^\frac{1}{I_s^c}
\end{equation}

\noindent A diferença crucial é que os dados utilizados no IPC-W não possuem a dimensão ``cidade". Se hipoteticamente os dados do IPC-W possuíssem tal dimensão, o processo de cálculo seria idêntico ao do IPC.

\subsection{Agregação do IPC ``nacional"}\label{ipc_3}

\noindent Para as agregações do IPC nacional, considera-se o peso de cada subitem $s$ referente à POF de cada cidade $c$, que denotaremos por $w_s^c$. Assim, calcula-se uma inflação para cada cidade $c$:

\begin{equation}
\pi_c=\frac{\sum_s{w_s^c \pi_s^c}}{\sum_s{w_s^c}}
\end{equation}

\noindent Por fim, o IBRE faz uso de um peso para cada cidade $c$, baseado na renda da mesma, peso este que denotaremos por $r_c$. Assim, calcula-se a inflação geral (IPC):

\begin{equation}
\pi_0=\frac{\sum_c{r_c \pi_c}}{\sum_c{r_c}}
\end{equation}

\subsection{Agregação do IPC de cada subitem}

\noindent A agregação para os IPC's de cada subitem vale dos mesmos pesos descritos na seção anterior. Note-se que não se trata de uma pura média ponderada pelos pesos das cidades, a POF de cada cidade também entra no cálculo da influência. A inflação do subitem $s$ é dada por:

\begin{equation}
\pi_0^s=\frac{\sum_c{r_c w_s^c\pi_s^c}}{\sum_c{r_c w_s^c}}
\end{equation}

%____ metodologia ipc-w (emap/ibre) ____%

\section{O cálculo do IPC-W (EMAp/IBRE)}\label{metodo_ipcw}

\noindent Os dados de preços online possuem essencialmente quatro dimensões:
\begin{itemize}
\item \textbf{website}: endereço eletrônico onde os preços foram coletados;
\item \textbf{sku} (\textit{stock keeping unit}): código de produto único em cada website;
\item \textbf{subitem}: classificação dos insumos com base na POF;
\item \textbf{data}.
\end{itemize}

\noindent \textbf{OBS}: Vale frisar que o IPC-W define ``insumo" como o par único website-sku, diferente da definição utilizada no cálculo do IPC. O mesmo ``arroz parboilizado Tio João 1kg" coletado em 2 websites diferentes são tratados como insumos diferentes no IPC-W. Portanto, na definição do IPC-W, os websites definem uma partição de todos os insumos. \textbf{Contudo, conforme demonstrado na \textit{Nota de equivalência}, não há perda de prejuízo nesta dimensão}. Ainda que todos os websites fossem tratados como informantes distintos, o cálculo geraria exatamente o mesmo resultado.\\

\noindent Tanto a coleta de preços quanto o cálculo do índice são feitos \textbf{uma vez por dia}. A inflação calculada representa uma \textbf{variação percentual mensal de preços}. O cross-section é definido pelo par website-sku (conforme já denominado \textbf{insumo}). O cálculo do IPC-W pode ser segmentado em seis etapas:

\begin{enumerate}
\item Coleta e estruturação da base de dados do IPC-W;
\item Classificação dos insumos nos subitens do IPC;
\item Filtro por \textit{missing data};
\item Cálculo dos preços médios e dos relativos;
\item Controle de \textit{outliers};
\item Agregação do IPC-W.
\end{enumerate}

\noindent De modo análogo ao IPC, utilizam-se aqui os mesmos conceitos de \textbf{período base} e \textbf{período referência}.\\

\subsection{Coleta e estruturação da base de dados do IPC-W}

Uma lista de \textbf{websites} para a coleta de preços é aprovada previamente pela equipe do projeto IPC-W. \textbf{Uma vez por dia}, é feita uma coleta de informações de \underline{todos} os \textbf{produtos} encontrados pelo sistema. Mais ainda, cada produto possui um código de identificação única dentro do website em que é apresentado, denominado \textbf{sku} (\textit{stock keeping unit}). O código sku varia de website para website, não sendo viável fazer uma correspondência entre diferentes websites para identificar produtos idênticos. Assim, o IPC-W define como \textbf{insumo} a chave de identificação única dos produtos coletados, formado pelo par website-sku. Em particular, produtos idênticos são tratados como insumos diferentes, se ofertados em websites diferentes. Os procedimentos de extração, limpeza e estruturação dos dados são melhor detalhados em outro relatório.\\

\noindent \textbf{Importante}: ao final dos passos acima, são \textbf{removidas da base de dados todas as informações coletadas em \textit{datas com problemas de coleta}}. Até o presente momento, tais datas consistem nos \color{red}primeiros 10 dias de coleta \color{black} (base reduzida e instável), e no dia \color{red}dd/mm/2017 \color{black} (houve queda nos servidores da FGV).

\subsection{Classificação dos insumos nos subitens do IPC}

\noindent Os websites associam seus produtos à \textbf{departamentos}, de modo que a base dados bruta herda esta mesma associação. Vale ressaltar que é possível que um mesmo produto (em um mesmo website) seja apresentado em mais de um departamento. As metodologias usuais (com preços coletados em gôndola) de cálculo de inflação classificam os produtos de modo distinto. Esta última é essencialmente baseada na POF, pesquisa realizada pelo IBGE. Esta mesma pesquisa serve de base para identificar qual \textbf{peso} é dado para cada classificação de produtos. Como os pesos consistem em informação imprescindível para o cálculo do índice, é imprescindível uma correspondência entre as diferentes associações de produtos aqui apresentadas (POF e departamentos de websites). Este é um problema de aprendizagem, solucionado em mais detalhes em outro relatório.\\

\subsection{Filtro por \textit{missing data}}\label{ipcw_miss}

\noindent Um dado online é considerado faltante (\textit{missing data}) em basicamente duas situações: (i) se está apresentado com preço zero, ou (ii) se o produto (já conhecido) não for encontrado pelo sistema de captura online. Ao estruturar a base de dados para o cálculo do índice em uma janela específica de tempo (do início do período base até o final do período referência), também pode ocorrer \textit{missing data} nas observações iniciais. Desta forma, existe uma terceira situação para o fenômeno: (iii) se o produto ainda não havia sido encontrado pelo sistema até aquela data.\\

\noindent Para fins de notação, denote por $P_{s,i}(t)$ o preço online do insumo $i$ do subitem $s$, coletado no dia $t$. Denote por $\mathcal{B}$ o conjunto de dias no período base, e $\mathcal{R}$ o conjunto de dias do período referência. Seja ainda $T_{s,i}(\mathcal{X})$ o número de observações que o insumo $i$ do subitem $s$ possui no período $\mathcal{X}=\mathcal{B},\mathcal{R}$, ou seja:

\begin{equation}
\begin{aligned}
T_{s,i}(\mathcal{X}) = \# \{t\in\mathcal{X}: P_{s,i}(t)\neq\textit{missing data}\} && ,\mathcal{X}=\mathcal{B},\mathcal{R}
\end{aligned}
\end{equation}

\noindent onde $\text{\#}A$ é o número de elementos do conjunto (finito) $A$. Com estes conceitos em mente, definimos os percentuais de dados faltantes para cada insumo $i$ do subitem $s$, em cada período:

\begin{equation}
\begin{aligned}
M_{s,i}(\mathcal{X}) = \frac{\# \mathcal{X}-T_{s,i}(\mathcal{X})}{\# \mathcal{X}} && ,\mathcal{X} = \mathcal{B}, \mathcal{R}
\end{aligned}
\end{equation}

\noindent Após os procedimentos das subseções anteriores, cria-se um filtro por observações faltantes (\textit{missing data}). Tal filtro em questão consiste em \textbf{manter} os insumos $i$ do subitem $s$ que satisfazem:

\begin{equation}
\text{max}\{M_{s,i}(\mathcal{X}):\mathcal{X} = \mathcal{B}, \mathcal{R}\} \leq 0.7
\end{equation}

\subsection{Cálculo dos preços médios e dos relativos}

\noindent Com os dados filtrados por \textit{missing data}, é calculado um preço médio para cada período, para cada insumo $i$ do subitem $s$:

\begin{equation}
\begin{aligned}
P_{s,i}(\mathcal{X})=\frac{1}{T_{s,i}(\mathcal{X})}\sum_{t \in \mathcal{X}}P_{s,i}(t) && ,\mathcal{X}=\mathcal{B},\mathcal{R}
\end{aligned}
\end{equation}

\noindent E com isto, calcula-se o relativo para cada insumo $i$ do subitem $s$:

\begin{equation}
R_{s,i} = \frac{P_{s,i}(\mathcal{R})}{P_{s,i}(\mathcal{B})}
\end{equation}

\subsection{Controle de \textit{outliers}}\label{ipcw_outliers}

\noindent O controle de \textit{outliers} é feito isoladamente para cada subitem. Portanto, para fins de ilustração, fixe-se o subitem $s$. Espera-se que os dados $\{R_{s,i}\}_i$ tenham distribuição log-normal. Desta forma, definindo $X_{s,i}=ln(R_{s,i})$, espera-se que os dados $\{X_{s,i}\}_i$ tenham distribuição normal. Seja $I_s$ o número de insumos do subitem $s$. Calcula-se a média e o desvio-padrão destes dados transformados:

\begin{equation}
\overline{X}_s=\frac{1}{I_s}\sum_i{X_{s,i}}
\end{equation}

\begin{equation}
\widehat{\sigma}_X(s)=\sqrt{\frac{1}{I_s-1}\sum_i{(X_{s,i}-\overline{X}_s)^2}}
\end{equation}

\noindent Finalmente, são \textbf{mantidos} apenas os insumos $i$ do subitem $s$ que satisfazem:

\begin{equation}
\overline{X}_s-3\widehat{\sigma}_X(s) \leq X_{s,i} \leq \overline{X}_s+3\widehat{\sigma}_X(s)
\end{equation}

\noindent A ideia por trás deste filtro é eliminar insumos que apresentaram uma variação de preços abnormal, se comparados com os demais insumos do mesmo subitem.

\subsection{Agregação do IPC-W}

\noindent Inicialmente é calculada um IPC-W para cada subitem $s$, valendo-se de uma média geométrica dos relativos:

\begin{equation}
\pi_{web}^s=\left(\prod_i{R_{s,i}}\right)^\frac{1}{I_s}-1
\end{equation}

\noindent Para a última agregação, é necessário considerar um peso para cada subitem. Estes pesos são obtidos em analogia à uma POF nacional (artificial), valendo-se dos pesos utilizados no cálculo do IPC (definições na seção \ref{ipc_3}):

\begin{equation}
w_s=\frac{\sum_c{r_c w_s^c}}{\sum_c{r_c}}
\end{equation}

\noindent Com esta definição, a inflação geral (IPC-W) é dada por:

\begin{equation}
\pi_{web}=\frac{\sum_s{w_s \pi_{web}^s}}{\sum_s{w_s}}
\end{equation}

\section{Melhorias e próximos passos}

\begin{itemize}
\item Quais são as datas com problemas de coleta? (10 dias iniciais + dias de queda nos servidores da FGV)
\item Em dias de problemas de coleta, deve-se (i) imputar \textit{missing data}, ou (ii) apagar o dia da base? A resposta para esta pergunta tem implicações no filtro de \textit{missing data} e em análises envolvendo \textit{missing data}.
\item Desenvolver método para obter pesos da POF sem intermédio do IBRE.
\item Descobrir metodologia utilizada pelo IBRE para a atualização dos pesos no cálculo da inflação. Com base nesta desenvolver a própria metodologia de atualização de pesos.
\end{itemize}

\end{document}